public class App {
    public static void main(String[] args) throws Exception {
        App task5050 = new App();

        //subtask 1 Đếm số lần xuất hiện của ký tự n trong chuỗi
        String str1 = "DCresource: JavaScript Exercises";
        char c1 = 'e';
        System.out.println("So lan xuat hien cua 1 phan tu trong chuoi la: " + task5050.countChar(str1, c1));

        //subtask 2 Loại bỏ các ký tự trắng ở đầu và cuối chuỗi
        String str2 = "   Hello World   ";
        String trimmedStr2 = str2.trim();
        System.out.println("Chuoi sau khi cat khoang trang 2 dau: " + trimmedStr2);

        //subtask 3 Loại bỏ chính xác chuỗi con ra khỏi chuỗi cho trước
        String originalString3 = "The quick brown fox jumps over the lazy dog";
        String newString3 = originalString3.replaceAll("the ", "");
        System.out.println(newString3);

        //subtask 4 Kiểm tra chuỗi sau có phải là kết thúc của chuỗi trước hay không
        String str41 = "Hello World";
        String str42 = "World";
        boolean result4 = str41.endsWith(str42);
        System.out.println("Kiem tra chuoi ket thuc: " + result4);

        //subtask 5 So sánh 2 chuỗi có giống nhau hay không
        String str51 = "Hello World";
        String str52 = "hellO worlD";
        boolean result5 = str51.equalsIgnoreCase(str52);
        System.out.println("So sanh chuoi co giong nhau khong: " + result5);

        //subtask 6 Kiểm tra ký tự thứ n của chuỗi có phải viết hoa hay không
        String str6 = "Hello World";
        int n6 = 0;
        char ch6 = str6.charAt(n6);
        boolean result6 = Character.isUpperCase(ch6);
        System.out.println("Kiem tra ky tu n cua chuoi co viet hoa ko: " + result6);

        //subtask 7 Kiểm tra ký tự thứ n của chuỗi có phải viết thường hay không
        String str7 = "Hello World";
        int n7 = 0;
        char ch7 = str7.charAt(n6);
        boolean result7 = Character.isLowerCase(ch7);
        System.out.println("Kiem tra ky tu n cua chuoi co viet thuong ko: " + result7);

        //subtask 8 Kiểm tra chuỗi trước có bắt đầu bằng chuỗi sau hay không
        String str81 = "Hello World";
        String str82 = "Hello";
        boolean result8 = str81.startsWith(str82);
        System.out.println("Kiem tra chuoi truoc co bat dau bang chuoi sau ko: " + result8);

        //subtask 9 Kiểm tra chuỗi đã cho có phải chuỗi rỗng hay không
        String str9 = "";
        boolean result9 = str9.isEmpty();
        System.out.println("Kiem tra chuoi co phai chuoi rong ko: " + result9);

        //subtask 10 Đảo ngược chuỗi
        // Trong đó str là chuỗi cần đảo ngược. Đầu tiên, bạn tạo một đối tượng StringBuilder từ chuỗi str. Sau đó, sử dụng phương thức reverse để đảo ngược chuỗi. Cuối cùng, sử dụng phương thức toString để chuyển đổi đối tượng StringBuilder trở lại thành chuỗi. Trong ví dụ trên, kết quả sẽ là “dlroW olleH” vì đó là chuỗi đảo ngược của chuỗi “Hello World”.
        String str10 = "Hello World";
        StringBuilder sb10 = new StringBuilder(str10);
        sb10.reverse();
        String reversedStr10 = sb10.toString();
        System.out.println("Chuoi sau khi dao nguoc: " + reversedStr10);
    }

    //subtask 1
    public int countChar(String str, char c) {
    int count = 0;
    for (int i = 0; i < str.length(); i++) {
        if (str.toLowerCase().charAt(i) == Character.toLowerCase(c)) {
            count++;
        }
    }
    return count;
}
}
